
let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];


const express = require("express");

const port = 3000;

const app = express();

app.use(express.json());

app.get("/home", (request, response) => {
	
	response.status(200).send("Hello User, Welcome to Homepage!")
});

app.get("/items", (request, response) => {

	response.status(201).send(items);
})

app.delete("/delete-item", (request, response) => {

	let deletedItem = items.pop()
	response.status(202).send(deletedItem)
})




app.listen(port, () => console.log(`Server is running at port ${port}`));